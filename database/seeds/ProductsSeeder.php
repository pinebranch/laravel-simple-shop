<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Huawei P30 Pro',                   
            'photo' => 'images/huawei-p30-pro.jpg',
            'price' => 1000000
        ]);

        DB::table('products')->insert([
            'name' => 'Apple iPhone 11',                    
            'photo' => 'images/iphone-11.jpg',
            'price' => 2000000
        ]);

        DB::table('products')->insert([
            'name' => 'Apple iPhone 11 Pro',
            'photo' => 'images/iphone-11-pro.jpg',
            'price' => 3000000
        ]);

        DB::table('products')->insert([
            'name' => 'Apple iPhone XS Max',
            'photo' => 'images/iphone-xs-max.jpg',
            'price' => 4000000
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung Galaxy Note 10',
            'photo' => 'images/samsung-galaxy-note-10.jpg',
            'price' => 5000000
        ]);

        DB::table('products')->insert([
            'name' => 'Samsung Galaxy S10+',
            'photo' => 'images/samsung-galaxy-s10-plus.jpg',
            'price' => 6000000
        ]);

        DB::table('products')->insert([
            'name' => 'Xiaomi Mi 9T',
            'photo' => 'images/xiaomi-mi-9t.jpg',
            'price' => 7000000
        ]);

        DB::table('products')->insert([
            'name' => 'Xiaomi Mi Note 10 Pro',
            'photo' => 'images/xiaomi-mi-note-10-pro.jpg',
            'price' => 8000000
        ]);
    }
}
