<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetails;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Trang danh sách đơn hàng
     *
     * @return void
     */
    public function index() 
    {
        $orders = Order::all();

        return view('orders', ['orders' => $orders]);
    }

    /**
     * Trang chi tiết đơn hàng (hóa đơn)
     *
     * @param int $id - id đơn hàng
     * @return void
     */
    public function orderDetails($id) 
    {
        $order = Order::find($id);
        
        if (!$order) {
            abort(404);
        }

        $orderDetails = OrderDetails::where('order_id', $id)->get();

        return view("order_details", ['order' => $order, 'orderDetails' => $orderDetails]);
    }
}
