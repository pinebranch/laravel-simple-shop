<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Cart extends Model
{
    protected $table = 'cart';

    protected $fillable = ['user_id', 'product_id', 'qty'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Các hàm dưới đây là static để thao tác với giỏ hàng
     * Trong dự án thực tế nên tách ra class riêng (CartHelper hoặc CartRepository...) để khỏi nhập nhằng với model
     */

    /**
     * Lấy danh sách trong giỏ hàng
     *
     * @return array Mảng sản phẩm trong giỏ hàng
     */
    public static function getCart() 
    {
        //nếu chưa có cart trong session thì tạo mới
        if (!session()->has('cart')) {
            $cart = [];
        } else {
            $cart = session()->get('cart');
        }        

        $userId = Auth::check() ? Auth::id() : 0;
        
        /**
         * Nếu user đã đăng nhập thì load cart của user trong db và đồng bộ với cart trong session.
         * Việc đồng bộ chỉ cần thực hiện 1 lần khi login
         * Sau đó thì các thao tác thêm vào giỏ hàng, cập nhật, xóa sản phẩm khỏi giỏ đều có cập nhật trực tiếp vào db và session nên không cần phải đồng bộ lại.
         * Sử dụng biến 'cart_synced' trong session để xác định đã đồng bộ hay chưa. 
         */
        if ($userId > 0) {
            if (session()->get('cart_synced', 0) == 0) {

                $cartDB = Cart::where('user_id', $userId)->with('product')->get();

                if (count($cartDB) > 0) {

                    //cập nhật từ db vào session
                    //lấy số lượng sản phẩm trong db thay vì trong cart
                    foreach ($cartDB as $item) {
                        if (!isset($cart[$item->product_id])) {
                            $cart[$item->product_id] = [
                                'name' => $item->product->name,
                                'photo' => $item->product->photo,
                                'price' => $item->product->price,
                                'qty' => $item->qty 
                            ];
                        } else {
                            $cart[$item->product_id]['qty'] = $item->qty;
                        }
                    }
                }

                //cập nhật từ session vào db                
                if (count($cart) > 0) {
                    
                    foreach ($cart as $id => $cartItem) {  

                        $isExisted = false;
                        foreach ($cartDB as $itemDb) {
                            if ($id == $itemDb->product_id) {
                                $isExisted = true;
                                break;
                            }
                        }

                        if (!$isExisted) {
                            $newItem = new Cart();
                            $newItem->user_id = $userId;
                            $newItem->product_id = $id;
                            $newItem->qty = $cartItem['qty'];
                            $newItem->save();
                        }
                    }
                }                

                session()->put('cart', $cart);
                session()->put('cart_synced', 1);
            }            
        }

        return $cart;
    }

    /**
     * Tính số sản phẩm trong giỏ hàng
     *
     * @return int
     */
    public static function getCartCount() 
    {
        $count = 0;

        $cart = Cart::getCart();

        foreach ($cart as $id => $item) {
            $count += $item['qty'];
        }
        
        return $count;
    }

    /**
     * Thêm vào giỏ hàng
     *
     * @param int $productId
     * @return void
     */
    public static function addToCart($productId)
    {
        $product = Product::find($productId);
        $userId = Auth::check() ? Auth::id() : 0;

        if (!$product) return;       

        $cart = Cart::getCart();

        if(isset($cart[$productId]))  //nếu đã có sản phẩm cùng id trong giỏ hàng thì tăng số lượng
        {
            $cart[$productId]['qty']++;
            
            //nếu user đã đăng nhập thì lưu vào db
            if ($userId > 0) {
                $cartItemDb = Cart::where('user_id', $userId)->where('product_id', $productId)->first();;
                $cartItemDb->qty ++;
                $cartItemDb->save();
            }
        } 
        else // chưa có sản phẩm trong giỏ hàng thì thêm mới với số lượng là 1
        {            
            $cart[$productId] = [
                'name' => $product->name,
                'photo' => $product->photo,
                'price' => $product->price,
                'qty' => 1
            ];

            //nếu user đã đăng nhập thì lưu vào db
            if ($userId > 0) {
                $cartItemDb = new Cart();
                $cartItemDb->user_id = $userId;
                $cartItemDb->product_id = $productId;
                $cartItemDb->qty = 1;
                $cartItemDb->save();
            }
        }

        //cập nhật lại giỏ hàng vào session
        session()->put('cart', $cart);
    }

    /**
     * Cập nhật số lượng sản phẩm trong giỏ hàng
     *
     * @param int $productId
     * @param int $qty
     * @return void
     */
    public static function updateCartItem($productId, $qty) 
    {
        $product = Product::find($productId);
        $userId = Auth::check() ? Auth::id() : 0;

        if (!$product) return;

        $cart = Cart::getCart();

        if (isset($cart[$productId])) {
            $cart[$productId]['qty'] = $qty;

            //nếu user đã đăng nhập thì lưu vào db
            if ($userId > 0) {
                $cartItemDb = Cart::where('user_id', $userId)->where('product_id', $productId)->first();
                $cartItemDb->qty = $qty;
                $cartItemDb->save();
            }

            session()->put('cart', $cart);
        }
    }

    /**
     * Xóa sản phẩm khỏi giỏ hàng
     *
     * @param int $productId
     * @return void
     */
    public static function removeCartItem($productId) 
    {
        $product = Product::find($productId);
        $userId = Auth::check() ? Auth::id() : 0;

        if (!$product) return;

        $cart = Cart::getCart();

        //xóa item khỏi mảng cart
        unset($cart[$productId]);

        //nếu user đã đăng nhập thì xóa khỏi db
        if ($userId > 0) {
            Cart::where('user_id', $userId)->where('product_id', $productId)->delete();
        }

        session()->put('cart', $cart);
    }

    /**
     * Xóa giỏ hàng (dùng sau khi đặt hàng xong)
     *
     * @return void
     */
    public static function emptyCart() {
        $userId = Auth::check() ? Auth::id() : 0;

        //nếu user đã đăng nhập thì xóa khỏi db
        if ($userId > 0) {
            Cart::where('user_id', $userId)->delete();
        }

        session()->put('cart', []);
    }
    
}
