<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->default(0);
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->decimal('total', 12, 0);
            $table->timestamps();
        });

        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->unsignedBigInteger('order_id'); //các cột increments là unsigned nên cột foreign key cũng phải là unsigned
            $table->unsignedBigInteger('product_id');
            $table->string('product_name');
            $table->string('product_price');
            $table->string('product_qty');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('order');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
        Schema::dropIfExists('order');
    }
}
