<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetails;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    /**
     * Trang thanh toán
     *
     * @return void
     */
    public function index() {

        if (!session()->has('cart')) {
            abort(404);
        }

        $cart = Cart::getCart();

        $total = 0;
        foreach ($cart as $item) {
            $total += $item['qty'] * $item['price'];
        }

        return view('checkout', ['cart' => $cart, 'total' => $total]);
    }

    /**
     * Action đặt hàng
     *
     * @param Request $request
     * @return void
     */
    public function process(Request $request) {

        $name = $request->input('order_name');
        $phone = $request->input('order_phone');
        $address = $request->input('order_address');
        
        $cart = Cart::getCart();
        $total = 0;
        foreach ($cart as $item) {
            $total += $item['qty'] * $item['price'];
        }

        $order = new Order();
        $order->name = $name;
        $order->phone = $phone;
        $order->address = $address;
        $order->total = $total;

        $order->save();

        if ($order->id > 0) {
            foreach ($cart as $id => $item) {
                $orderDetails = new OrderDetails();
                $orderDetails->order_id = $order->id;
                $orderDetails->product_id = $id;
                $orderDetails->product_name = $item['name'];
                $orderDetails->product_price = $item['price'];
                $orderDetails->product_qty = $item['qty'];
                $orderDetails->save();
            }

            //xóa giỏ hàng sau khi đặt hàng xong
            Cart::emptyCart();
        }

        return redirect()->route('home')->with('status', 'Đơn hàng đã được tạo.'); 
    }
}
