<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Trang giỏ hàng
     *
     * @return void
     */
    public function index()
    {
        if (!session()->has('cart')) {
            $cart = [];
        } else {
            $cart = session()->get('cart');
        }        

        $total = 0;
        foreach ($cart as $item) {
            $total += $item['qty'] * $item['price'];
        }

        return view('cart', ['cart' => $cart, 'total' => $total]);
    }

    /**
     * Action thêm vào giỏ hàng
     * Method: POST
     *
     * @param Request $request
     * @return void
     */
    public function addToCart(Request $request) {

        $productId = $request->input('productId');
        $product = Product::find($productId);

        if(!$product) {
            abort(404);
        }        

        //Kiểm tra nếu chưa có cart trong session thì tạo cart mới
        if(!session()->has('cart')) {
            $cart[$productId] = [
                'name' => $product->name,
                'photo' => $product->photo,
                'price' => $product->price,
                'qty' => 1
            ];

            session()->put('cart', $cart);
            return redirect()->back()->with('status', 'Sản phẩm đã được thêm vào giỏ hàng.');
        }

        $cart = session()->get('cart');

        if(isset($cart[$productId]))  //nếu đã có sản phẩm cùng id trong giỏ hàng thì tăng số lượng
        {
            $cart[$productId]['qty']++;
        } 
        else // chưa có sản phẩm trong giỏ hàng thì thêm mới với số lượng là 1
        {            
            $cart[$productId] = [
                'name' => $product->name,
                'photo' => $product->photo,
                'price' => $product->price,
                'qty' => 1
            ];
        }

        //cập nhật lại giỏ hàng vào session
        session()->put('cart', $cart);

        return redirect()->back()->with('status', 'Sản phẩm đã được thêm vào giỏ hàng.');
    }

    /**
     * Action cập nhật/xóa sản phẩm trong giỏ hàng
     * Method: POST
     *
     * @param Request $request
     * @return void
     */
    public function updateCart(Request $request) {
        
        if (!session()->has('cart')) {
            abort(404);
        }

        //lấy thông tin nút bấm nào được submit (cập nhật số lượng hay xóa sản phẩm)
        $submitButton = $request->input('submit_button');

        $productId = $request->input('productId');
        $qty = $request->input('qty');
        
        $cart = session()->get('cart');

        //nếu nhấn cập nhật mà số lượng <= 0 thì coi như là xóa
        if ($qty <= 0) {
            $submitButton = 'remove';
        }

        switch ($submitButton) {
            case "update": //cập nhật số lượng sản phẩm                               
                if (isset($cart[$productId])) {
                    $cart[$productId]['qty'] = $qty;
                    session()->put('cart', $cart);
                }
            break;

            case "remove": //xóa khỏi giỏ hàng
                unset($cart[$productId]);
                session()->put('cart', $cart);
            break;
        }

        return redirect()->back()->with('status', 'Giỏ hàng đã được cập nhật.');        
    }    
}
