<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    protected $attributes = [
        'user_id' => 0, //đặt user_id mặc định là 0 khi insert/update (chỉ dùng demo)
    ];
}
