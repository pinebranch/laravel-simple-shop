@extends('layouts.app')

@section('content')
<div class="container">
    @if (session()->has('status'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('status') }}
        </div>
    @endif
    <h1>Đơn hàng</h1>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tên</th>
                <th>Điện thoại</th>
                <th>Địa chỉ</th>
                <th>Tổng cộng</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->phone }}</td>
                <td>{{ $item->address }}</td>
                <td>
                    {{ number_format($item->total, 0) }}
                    <a href="/order-details/{{ $item->id }}" class="ml-3 btn btn-sm btn-primary">In hóa đơn</a>
                </td>                
            </tr>
            @endforeach
        </tbody>
    </table>
        
</div>
@endsection
