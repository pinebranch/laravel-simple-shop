## Demo trang shop đơn giản

## Installation

Chạy lệnh install composer và npm

	composer install

    npm install

    npm run dev

Cấu hình db trong .evn

    DB_DATABASE=db_name
    DB_USERNAME=root
    DB_PASSWORD=password

Chạy lệnh để update cấu hình

    php artisan config:cache

Tạo bảng trong db

    php artisan migrate

Tạo data mẫu

    php artisan db:seed --class=ProductsSeeder

Chạy trang web (port tùy ý)

    php artisan serve --port=81

Địa chỉ web

    http://localhost:81

## Ghi chú

Demo có 2 phiên bản: dùng db + session cho cart và chỉ dùng session

* Dùng db + session: phiên bản hiện tại
* Chỉ dùng session: các file để trong thư mục Backup trong controller và view. Nếu muốn dùng bản chỉ có session thì thay các file controller và view trong các thư mục backup
