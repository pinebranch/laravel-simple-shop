@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-6">
            <h2>Thông tin giao hàng</h2>
            <form method="POST" action="/checkout/process">
                @csrf
                <div class="form-group">
                    <label>Họ tên</label>
                    <input type="text" name="order_name" required class="form-control" />
                </div>
                <div class="form-group">
                    <label>Điện thoại</label>
                    <input type="text" name="order_phone" required class="form-control" />
                </div>
                <div class="form-group">
                    <label>Địa chỉ</label>
                    <input type="text" name="order_address" required class="form-control" />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Thanh toán</a>
                </div>
            </form>
            
        </div> {{-- col --}}

        <div class="col-md-6">
            <h2>Giỏ hàng</h2>
            <div id="cart" class="mb-3 checkout">
                <div class="row cart-header">
                    <div class="col-md-6">Sản phẩm</div>
                    <div class="col-md-2">Giá</div>
                    <div class="col-md-2">Số lượng</div>
                    <div class="col-md-2 text-right">Thành tiền</div> 
                    
                </div>

                @foreach($cart as $id => $item)
                <div class="row cart-item">
                    <div class="col-md-6">
                        <div class="media">
                            <img class="mr-3 cart-item-image" src="/{{ $item['photo'] }}" />
                            <div class="media-body">
                                <p class="mt-0">{{ $item['name'] }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">{{ number_format($item['price'], 0) }}</div>
                    <div class="col-md-2">{{ number_format($item['qty'], 0) }}</div>
                    <div class="col-md-2 text-right">{{ number_format($item['price'] * $item['qty'], 0) }}</div> 
                </div>          
                    
                @endforeach
                <div class="row cart-item">
                    <div class="col-md-12 text-right"><strong>Tổng cộng: {{ number_format($total, 0) }}</strong></div>
                </div>
            </div>
        </div> {{-- #cart --}}
    </div> {{-- row --}}
</div>
@endsection
