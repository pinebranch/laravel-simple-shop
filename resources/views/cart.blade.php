@extends('layouts.app')

@section('content')
<div class="container">
    @if (session()->has('status'))
    <div class="alert alert-success" role="alert">
        {{ session()->get('status') }}
    </div>
    @endif

    @if (count($cart) <= 0)
        <div class="alert alert-info" role="alert">
            Chưa có sản phẩm trong giỏ hàng
        </div>
    @else

    <div id="cart" class="mb-3">
        <div class="row cart-header">
            <div class="col-md-5">Sản phẩm</div>
            <div class="col-md-2">Giá</div>
            <div class="col-md-1">Số lượng</div>
            <div class="col-md-2 text-right">Thành tiền</div> 
            <div class="col-md-2"></div>
        </div>
        @foreach($cart as $id => $item)
        <form method="POST" action="/cart/update" class="row cart-item">
            @csrf
            <input type="hidden" name="productId" value="{{ $id }}" />
            <div class="col-md-5">
                <div class="media">
                    <img class="mr-3 cart-item-image" src="/{{ $item['photo'] }}" />
                    <div class="media-body">
                        <p class="mt-0">{{ $item['name'] }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2">{{ number_format($item['price'], 0) }}</div>
            <div class="col-md-1"><input type="number" name="qty" value="{{ $item['qty'] }}" class="form-control" /></div>
            <div class="col-md-2 text-right">{{ number_format($item['price'] * $item['qty'], 0) }}</div> 
            <div class="col-md-2">

                {{-- trong 1 form có 2 action khác nhau --> tạo 2 button cùng name, khác value, khi submit sẽ dựa vô value để xác định thao tác cần xử lý --}}
                <button type="submit" name="submit_button" value="update" class="btn btn-success btn-sm">Cập nhật</button>
                <button type="submit" name="submit_button" value="remove" class="btn btn-danger btn-sm">Xóa</button>
                
            </div>
            
        </form>
        @endforeach
        <div class="row cart-item">
            <div class="col-md-10 text-right"><strong>Tổng cộng: {{ number_format($total, 0) }}</strong></div>
        </div>
    </div>

    <div class="row">
        <div class="col"><a href="/" class="btn btn-primary">Tiếp tục mua hàng</a></div>
        <div class="col text-right"><a href="/checkout" class="btn btn-primary">Thanh toán</a></div>
    </div>

    @endif
</div>
@endsection
