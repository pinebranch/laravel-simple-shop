<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Trang giỏ hàng
     *
     * @return void
     */
    public function index()
    {
        $cart = Cart::getCart();        

        $total = 0;
        foreach ($cart as $item) {
            $total += $item['qty'] * $item['price'];
        }

        return view('cart', ['cart' => $cart, 'total' => $total]);
    }

    /**
     * Action thêm vào giỏ hàng
     * Method: POST
     *
     * @param Request $request
     * @return void
     */
    public function addToCart(Request $request) {

        $productId = $request->input('productId');
        $product = Product::find($productId);

        if(!$product) {
            abort(404);
        }        

        Cart::addToCart($productId);

        return redirect()->back()->with('status', 'Sản phẩm đã được thêm vào giỏ hàng.');
    }

    /**
     * Action cập nhật/xóa sản phẩm trong giỏ hàng
     * Method: POST
     *
     * @param Request $request
     * @return void
     */
    public function updateCart(Request $request) {
        
        if (!session()->has('cart')) {
            abort(404);
        }

        //lấy thông tin nút bấm nào được submit (cập nhật số lượng hay xóa sản phẩm)
        $submitButton = $request->input('submit_button');

        $productId = $request->input('productId');
        $qty = $request->input('qty');

        //nếu nhấn cập nhật mà số lượng <= 0 thì coi như là xóa
        if ($qty <= 0) {
            $submitButton = 'remove';
        }

        switch ($submitButton) {
            case "update": //cập nhật số lượng sản phẩm
                Cart::updateCartItem($productId, $qty);
            break;

            case "remove": //xóa khỏi giỏ hàng
                Cart::removeCartItem($productId);
            break;
        }

        return redirect()->back()->with('status', 'Giỏ hàng đã được cập nhật.');        
    }    
}
