<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Override method logout để giữ lại session cart khi logout
     * Mặc định khi logout, tất cả session đều bị xóa
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        $cart = session()->get('cart');

        $this->guard()->logout();

        $request->session()->invalidate();

        session()->put('cart', $cart);

        return $this->loggedOut($request) ?: redirect('/');
    }

}
