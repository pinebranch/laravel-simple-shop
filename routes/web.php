<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductsController;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/cart/', [CartController::class, 'index'])->name('cart');
Route::post('/cart/add', [CartController::class, 'addToCart'])->name('cart_add');
Route::post('/cart/update', [CartController::class, 'updateCart'])->name('cart_update');

Route::get('/checkout', [CheckoutController::class, "index"])->name('checkout');
Route::post('/checkout/process', [CheckoutController::class, "process"])->name('checkout_process');

Route::get('/orders', [OrderController::class, 'index'])->name('orders');
Route::get('/order-details/{id}', [OrderController::class, 'orderDetails'])->name('order_details');
