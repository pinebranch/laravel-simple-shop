

    $('document').ready(function() {

        $('.btn-add-to-cart').on('click', function(e) {
            e.preventDefault();
            var $this = $(this);
            var productId = $this.attr('data-id');

            axios.post('/product/add-to-cart', {
                'productId': productId
            })
            .then(function (response) {
                console.log(response);
            });
        });

    })