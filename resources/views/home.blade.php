@extends('layouts.app')

@section('content')
<div class="container">
    @if (session()->has('status'))
        <div class="alert alert-success" role="alert">
            {{ session()->get('status') }}
        </div>
    @endif
    
    <div class="row">
        @foreach ($products as $item)
            <div class="col-md-3 mb-4">
                <div class="card" >
                    <img src="/{{ $item->photo }}" class="card-img-top p-3" />
                    <div class="card-body">
                        <h5 class="card-title">{{ $item->name }}</h5>
                        <p class="product-price text-danger text-bold">{{ number_format($item->price, 0) }}</p>
                        <form method="POST" action="/cart/add">
                            @csrf
                            <input type="hidden" name="productId" value="{{ $item->id }}" />
                            <button type="submit" class="btn btn-primary">Thêm vào giỏ</a>
                        </form>
                        
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
