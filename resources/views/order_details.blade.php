<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hóa đơn</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">        
    </head>
    <body>

        {{-- Trang này không sử dụng layout chung, vì có chức năng in nên cần layout riêng --}}

        <div class="container">

            <h1>HÓA ĐƠN</h1>

            <p><strong>Họ tên: </strong> {{ $order->name }}</p>
            <p><strong>Điện thoại: </strong> {{ $order->phone }}</p>
            <p><strong>Địa chỉ: </strong> {{ $order->address }}</p>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Sản phẩm</th>
                        <th>Đơn giá</th>
                        <th>Số lượng</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $index = 1; ?>
                    @foreach ($orderDetails as $item)
                    <tr>
                        <td>{{ $index }}</td>
                        <td>{{ $item->product_name }}</td>
                        <td>{{ number_format($item->product_price, 0) }}</td>
                        <td>{{ $item->product_qty }}</td>
                        <td>
                            {{ number_format($item->product_price * $item->product_qty, 0) }}
                        </td>                
                    </tr>
                    <?php $index++; ?>
                    @endforeach
                    <tr>
                        <td colspan="5" class="text-right">Tổng cộng: {{ number_format($order->total) }}</td>
                    </tr>
                </tbody>
            </table>

        </div>

        <script>
            window.print(); // thực hiện chức năng in của trình duyệt
        </script>
    </body>
</html>

